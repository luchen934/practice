const Qromise = require('../src/promise');


test('promise', () => {
    new Qromise((resolve, reject) => {
        setTimeout(() => {
            resolve('111');
        }, 0);
    }).then(function () {
        console.log('--ok--');
    });
    expect(1).toEqual(1);
})