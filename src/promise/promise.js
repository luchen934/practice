const PENDING = 'pending'; //进行中
const FULFILLED = 'fulfilled'; //成功
const REJECTED = 'rejected'; //失败

const isInBrowser = typeof window !== 'undefined';

// 在第一版promise实现中 使用了setTimeout 来实现异步执行
// 但是通过promise加入任务队列的任务 具备微任务优先的特性 相比较于普通 settimeout 加入的任务要优先执行
const nextTick = function(nextTickHandler) {
    if (isInBrowser) {
        if (typeof MutationObserver !== 'undefined') { // 首选 MutationObserver
            let counter = 1;
            let observer = new MutationObserver(nextTickHandler); // 声明 MO 和回调函数
            let textNode = document.createTextNode(counter);
            observer.observe(textNode, { // 监听 textNode 这个文本节点
                characterData: true // 一旦文本改变则触发回调函数 nextTickHandler
            });
            const start = function () {
                counter = (counter + 1) % 2; // 每次执行 timeFunc 都会让文本在 1 和 0 间切换
                textNode.data = counter;
            };
            start();
        } else {
            setTimeout(nextTickHandler, 0);
        }
    } else {
        process.nextTick(nextTickHandler);
    }
};

// 和promise区分换了下字母
// https://jiawei397.github.io/docs/note/js/es6/promisePolyfill.html#%E6%AD%A5%E9%AA%A4%E4%BA%8C-%E5%AE%9E%E7%8E%B0then%E6%96%B9%E6%B3%95
class Qromise {
    constructor(fn) {
        this.status = PENDING;
        this.value = undefined;
        this.reason = undefined;
        // 
        this.onFulfilledList = [];
        this.onRejectedList = [];
        const resolve = (val) => {
            if (this.status !== PENDING) { // 参见第4条，状态变化后就不能再修改了
                return;
            }
            this.status = FULFILLED;
            this.value = val;
            //todo
            this.onFulfilledList.forEach((cb) => cb && cb.call(this, val));
            this.onFulfilledList = [];
        };

        const reject = (err) => {
            if (this.status !== PENDING) { // 参见第4条，状态变化后就不能再修改了。
                return;
            }
            this.status = REJECTED;
            this.reason = err;
            //todo
            this.onRejectedList.forEach((cb) => cb && cb.call(this, err));
            this.onRejectedList = [];
        };
        try {
            fn(resolve, reject);
        } catch (e) {
            reject(e);
        }
    }

    then(onFulfilled, onRejected) {
        return new Qromise((resolve, reject) => {
            const onResolvedFunc = function (val) {
                const cb = function () {
                    try {
                        if (typeof onFulfilled !== 'function') { // 如果成功了，它不是个函数，意味着不能处理，则把当前Promise的状态继续向后传递
                            resolve(val);
                            return;
                        }
                        const x = onFulfilled(val);
                        resolve(x);
                    } catch (e) {
                        reject(e);
                    }
                };
                nextTick(cb, 0);
            };
    
            const onRejectedFunc = function (err) {
                const cb = function () {
                    try {
                        if (typeof onRejected !== 'function') { // 如果失败了，它不是个函数，意味着不能处理，则把当前Promise的状态继续向后传递
                            reject(err);
                            return;
                        }
                        const x = onRejected(err);
                        resolve(x); //处理了失败，则意味着要返回的新的promise状态是成功的
                    } catch (e) {
                        reject(e);
                    }
                };
                nextTick(cb, 0);
            };
    
            if (this.status === PENDING) {
                this.onFulfilledList.push(onResolvedFunc);
                this.onRejectedList.push(onRejectedFunc);
            } else if (this.status === FULFILLED) {
                onResolvedFunc(this.value)
            } else {
                onRejectedFunc(this.reason)
            }
        });
    }

    catch(onRejected) {
        return this.then(null, onRejected);
    }

}

Promise.all = function (arr) {
    const Constructor = this;
    if (!Array.isArray(arr)) {
        return Constructor.reject(new TypeError('You must pass an array to all.'));
    }
    let counter = arr.length;
    if (counter === 0) {
        return Constructor.resolve(arr);
    }
    return new Constructor((resolve, reject) => {
        const result = [];
        arr.forEach((promise, i) => {
            Constructor.resolve(promise) //这是为了防止参数并非Promise处理的
                .then((val) => {
                    result[i] = val;
                    counter--;
                    if (counter === 0) {
                        resolve(result);
                    }
                }, reject);
        });
    });
};

export { Qromise }